# NEW_AR_Lite_Mobile

![VR app screenshot of a virtual office space](Saved/AutoScreenshot.png)

Simulated augmented reality within virtual reality, allowing interaction with plant models pulled from a computer screen.

Developed with Unreal Engine 4.
